function searchMarkets(){
    var name = $(".search-market").val();

    $(".search-market-button").html('<i class="fa fa-spin fa-spinner"></i>');

    getMarkets(name);
}

function searchProducts(){
    var name = $(".search-product").val();

    $(".search-product-button").html('<i class="fa fa-spin fa-spinner"></i>');
    $("#products-list").empty();

    getMarketProducts(name);
}

function addToFavorites(obj){
    if($(obj).hasClass('favorite-product')){
        return removeFromFavorites(obj);
    }

    let product_id = $(obj).attr('product_id');
    let current_button_html = $(obj).html();

    $.ajax({
        url: "/add_to_favorites",
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        data: { product_id : product_id },
        beforeSend(){
            $(obj).html('<i class="fa fa-spin fa-spinner"></i>');
        },
        success: function(response){
            if(response.success){
                toastr.success(response.message);

                $(obj).addClass('favorite-product');
            }else{
                if(response.cart){
                    toastr.error(response.message);
                }else{
                    toastr.info('Você precisa efetuar login ou criar sua conta para adicionar produtos aos favoritos.');
                }
            }
        },
        error: function(response){
            if(response.success){
                toastr.success(response.message);

                $(obj).addClass('favorite-product');
            }else{
                if(response.cart){
                    toastr.error(response.message);
                }else{
                    toastr.info('Você precisa efetuar login ou criar sua conta para adicionar produtos aos favoritos.');
                }
            }
        },
        complete: function(){
            $(obj).html(current_button_html);
        }
    });
}

function removeFromFavorites(obj){
    let product_id = $(obj).attr('product_id');
    let current_button_html = $(obj).html();

    $.ajax({
        url: "/remove_from_favorites",
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        data: { product_id : product_id },
        beforeSend(){
            $(obj).html('<i class="fa fa-spin fa-spinner"></i>');
        },
        success: function(response){
            if(response.success){
                toastr.success(response.message);

                $(obj).removeClass('favorite-product');
            }else{
                if(response.cart){
                    toastr.error(response.message);
                }else{
                    console.log(response);
                    toastr.info('Você precisa efetuar login ou criar sua conta para remover produtos dos favoritos.');
                }
            }
        },
        error: function(response){
            if(response.success){
                toastr.success(response.message);

                $(obj).removeClass('favorite-product');
            }else{
                if(response.cart){
                    toastr.error(response.message);
                }else{
                    console.log(response);
                    toastr.info('Você precisa efetuar login ou criar sua conta para remover produtos dos favoritos.');
                }
            }
        },
        complete: function(){
            $(obj).html(current_button_html);
        }
    });
}

function addProductToCart(obj){
    let product_id = $(obj).attr('product_id');
    let market_id = $(obj).attr('market_id');
    let quantity = $(obj).closest('.cart-buttons').find('input').val();
    let current_button_html = $(obj).html();

    $.ajax({
        url: "/add_product_to_cart",
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        data: {product_id : product_id, market_id : market_id, quantity : quantity},
        beforeSend(){
            $(obj).html('<i class="fa fa-spin fa-spinner"></i>');
        },
        success: function(response){
            if(response.success){
                toastr.success(response.message);
                $(obj).closest('.modal').modal('hide');

                cart_products = response.cart;

                refreshCart();
            }else{
                if(response.cart || response.invalid_market){
                    toastr.error(response.message);
                }else{
                    toastr.info('Você precisa efetuar login ou criar sua conta para adicionar produtos ao carrinho.');
                }
            }
        },
        error: function(response){
            if(response.success){
                toastr.success(response.message);
                $(obj).closest('.modal').modal('hide');

                cart_products = response.cart;

                refreshCart();
            }else{
                if(response.cart || response.invalid_market){
                    toastr.error(response.message);
                }else{
                    toastr.info('Você precisa efetuar login ou criar sua conta para adicionar produtos ao carrinho.');
                }
            }
        },
        complete: function(){
            $(obj).html(current_button_html);
        }
    });
}

function removeProductFromCart(obj, delete_div = null){
    let cart_id = $(obj).attr('cart_id');

    $.ajax({
        url: "/remove_product_from_cart",
        method: 'POST',
        headers: {
            'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
        },
        data: {cart_id : cart_id},
        beforeSend() {
            $(obj).html('<i class="fa fa-spin fa-spinner"></i>');
        },
        success: function(response){
            if(response.success){
                toastr.success(response.message);

                cart_products = response.cart;

                refreshCart();
            }else{
                if(response.cart){
                    toastr.error(response.message);
                }else{
                    console.log(response);
                }

            }
        },
        error: function(response){
            if(response.success){
                toastr.success(response.message);

                cart_products = response.cart;

                refreshCart();
            }else{
                if(response.cart){
                    toastr.error(response.message);
                }else{
                    console.log(response);
                }

            }
        },
        complete: function(){
            if(delete_div){
                $(delete_div).remove();

                if(cart_products.length == 0){
                    document.location.reload();
                }
            }
        }
    });
}

function refreshCart(){
    if(cart_products.length > 0){
        $("#cart-products-container").empty();

        $.each(cart_products, function(index, result){
            prepareCartProductObject(result);

            createElementFromTemplate($("#cart-product-template"), result, $("#cart-products-container"));
        });

    }else{
        $("#cart-products-container").html('<p class="mb-4">Nenhum produto adicionado ao carrinho ainda.</p>');
    }

    refreshCartTotals();
}

function prepareCartProductObject(result){
    result.product.media[0] ? result.photo = result.product.media[0].url : undefined;
    result.name = result.product.name;
    result.quantity_price = '<span class="count">' + result.quantity + 'x</span> ' + result.product.price.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

    return result;
}
function refreshCartTotals(){
    let subtotal = 0;
    let quantity = 0;
    let items_text = 'x item';

    $.each(cart_products, function(index, p){
        quantity += parseInt(p.quantity);
        subtotal += parseFloat((p.product.price * p.quantity));
    });

    if(quantity == 0){
        items_text = ' itens';
    }
    if(quantity > 1){
        items_text = 'x itens';
    }

    $("#cart-subtotal").text(subtotal.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));
    $("#cart-totals").html(quantity + items_text + ' - ' + subtotal.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"}));
}

function prepareProductObject(result, load_categories = true){
    if(load_categories){
        result.category_name = result.category.name;
        result.category_link = window.location.origin + '/market/' + market.id + '/categories/' + result.category_id;
    }

    if(favorites.length > 0){
        let favorite_product = favorites.find(favorites => favorites === result.id);

        result.favoriteClass = favorite_product ? 'favorite-product' : null;
    }else{
        result.favoriteClass = null;
    }

    result.link = window.location.origin + '/product/' + result.id;
    result.media[0] ? result.photo = result.media[0].url : undefined;
    result.price = result.price.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});
    result.discount_price = result.discount_price.toLocaleString("pt-BR", { style: "currency" , currency:"BRL"});

    return result;
}

function getCategoryName(category, category_id) {
    return category.id === category_id;
}

function updateQtySelector(){
    $('.pro-qty').append('<a href="javascript:void(0)" class="inc qty-btn">+</a>');
    $('.pro-qty').append('<a href="javascript:void(0)" class= "dec qty-btn">-</a>');

    $('.qty-btn').unbind();
    $('.qty-btn').on('click', function (e) {
        e.preventDefault();

        var $button = $(this);
        var oldValue = $button.parent().find('input').val();
        if ($button.hasClass('inc')) {
            var newVal = parseFloat(oldValue) + 1;
        } else {
            // Don't allow decrementing below zero
            if (oldValue > 0) {
                var newVal = parseFloat(oldValue) - 1;
            } else {
                newVal = 0;
            }
        }
        $button.parent().find('input').val(newVal);
    });
}

function changeUserType(){
    let type = $("#legal-type-select").val();

    if(type == 1){
        $("#document-label").text('CPF');
        $("#document-input").mask('000.000.000-00');
    }else{
        $("#document-label").text('CNPJ');
        $("#document-input").mask('00.000.000/0000-00');
    }
}

$(document).ready(function(){
    toastr.options = {
        "debug": false,
        "positionClass": "toast-bottom-left",
    }

    $(".cpf_mask").mask('000.000.000-00');
    $(".cnpj_mask").mask('00.000.000/0000-00');
    $(".cep_mask").mask('00000-000');
    $(".phone_mask").mask('(00) 00000-0000');

    refreshCart();
    changeUserType();

    $("#loading").hide();

    $('[data-target="#quick-view-modal-container"]').on('click', function(){
        let product_id = $(this).attr('product_id');

        $.ajax({
            url: 'https://imarket.digital/api/products/' + product_id,
            async: false,
            success: function(response){
                if(response.success){
                    let result = prepareProductObject(response.data, false);

                    if(cart_products.length > 0){
                        let actual_cart_product = cart_products.find(cart_products => cart_products.product_id === parseInt(product_id));

                        result.quantity = actual_cart_product ? actual_cart_product.quantity : 1;
                        result.addToCartButtonText = actual_cart_product ? '<i class="fa fa-shopping-cart"></i> Atualizar carrinho' : '<i class="fa fa-shopping-cart"></i> Adicionar ao carrinho';
                    }

                    $("#product-modal-details-container").empty();

                    createElementFromTemplate($("#product-modal-details-template"), result, $("#product-modal-details-container"));
                    updateQtySelector();
                }
            }
        });
    });
})

function updateCount(count){
    $('.page-count').text(count);
}

function updateTotalCount(count){
    $('.page-total-count').text(count);
}

function getMarketProducts(search_name = null){
    var search = 'market_id:' + market.id;
    var searchFields = 'market_id:=';

    if(category_id){
        search += ';category_id:' + category_id;
        searchFields += ';category_id:=';
    }
    if(search_name){
        search += ';name:' + search_name;
        searchFields += ';name:like';
    }

    var products_url = 'https://imarket.digital/api/products/?with=category&search=' + search + '&searchFields=' + searchFields + '&searchJoin=and&limit=12';
    var products_count_url = 'https://imarket.digital/api/products/count?search=' + search + '&searchFields=' + searchFields + '&searchJoin=and';

    $.ajax({
        url: products_url,
        async: false,
        success: function(response){
            if(response.success){
                products = response.data;
                products_count = products.length;

                updateCount(products_count);

                if(response.data.length > 0){
                    $.each(response.data, function(index, result){
                        result = prepareProductObject(result);

                        createElementFromTemplate($("#product-template"), result, $("#products-list"));
                    });
                }else{
                    $("#products-list").html('<div class="bg-white w-100 p-4"><h4>Nenhum produto encontrado.</h4></div>')
                }
            }
        },
        complete: function(){
            $(".search-product-button").html('<i class="fa fa-check"></i>');
        }
    });

        // Contador de produtos
    $.ajax({
        url: products_count_url,
        async: false,
        success: function(response){
            if(response.success){
                products_count = response.data;

                updateTotalCount(products_count);
            }
        }
    });
}

function getMarkets(search_name = null){
    var search = '';

    if(search_name){
        search = '?search=name:' + search_name + '&searchFields=name:like';
    }

    $.ajax({
        url: 'https://imarket.digital/api/markets' + search,
        async: false,
        success: function(response){
            if(response.success){
                if(response.data.length > 0){
                    if(search_name){
                        $("#markets-list").slick('unslick');
                        $("#markets-list").html('');
                    }

                    $.each(response.data, function(index, result){
                        result.link = window.location.origin + '/change-market/' + result.id;
                        result.media[0] ? result.photo = result.media[0].url : undefined;

                        createElementFromTemplate($("#market-template"), result, $("#markets-list"));
                    });

                    tabSlider();
                }else{
                    $("#markets-list").html('<div class="card"><div class="card-body"><h4 class="mb-0">Nenhum mercado encontrado em sua região.</h4></div></div>');
                }
            }
        },
        complete: function(){
            $(".search-market-button").html('<i class="fa fa-check"></i>');
        }
    });
}

function tabSlider(){
    var tabSlider = $('#markets-list');

    tabSlider.slick({
        arrows: true,
        autoplay: false,
        dots: false,
        infinite: false,
        slidesToShow: 3,
        prevArrow: '<button type="button" class="slick-prev"><i class="fa fa-caret-left"></i></button>',
        nextArrow: '<button type="button" class="slick-next"><i class="fa fa-caret-right"></i></button>',
        responsive: [{
            breakpoint: 1499,
            settings: {
                slidesToShow: 3,
            }
        },
            {
                breakpoint: 1199,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 991,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 767,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 575,
                settings: {
                    slidesToShow: 1,
                }
            },
            {
                breakpoint: 479,
                settings: {
                    slidesToShow: 1,
                }
            }
        ]
    });
}
