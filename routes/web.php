<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('', 'WebsiteController@index')->name('index');
Route::get('login_register', 'WebsiteController@loginRegister')->name('login-register');
Route::get('markets-list', 'WebsiteController@marketsList')->name('markets-list');
Route::get('change-market/{market?}', 'WebsiteController@changeMarket')->name('change-market');
Route::get('market/{market}', 'WebsiteController@market')->name('market');
Route::get('market/{market}/categories/{category}', 'WebsiteController@market')->name('market.category');
Route::get('cart', 'WebsiteController@cart')->name('cart');
Route::get('checkout', 'WebsiteController@checkout')->name('checkout');
Route::get('logout', 'WebsiteController@logout')->name('logout');

Route::get('profile', 'WebsiteController@profile')->name('profile');
Route::get('favorites', 'WebsiteController@favorites')->name('favorites');

Route::post('login', 'WebsiteController@login')->name('login');
Route::post('update_user', 'WebsiteController@updateUser')->name('update_user');
Route::post('register', 'WebsiteController@register')->name('register');
Route::post('register_address', 'WebsiteController@register_address')->name('register_address');
Route::post('add_to_favorites', 'WebsiteController@addProductToFavorites')->name('add_to_favorites');
Route::post('remove_from_favorites', 'WebsiteController@removeProductFromFavorites')->name('remove_from_favorites');
Route::post('add_product_to_cart', 'WebsiteController@addProductToCart')->name('add_product_to_cart');
Route::post('remove_product_from_cart', 'WebsiteController@removeProductFromCart')->name('remove_product_from_cart');

Route::get('checkout/mercadopago/{api_token}/{delivery_address_id}', 'WebsiteController@mercadopago_app')->name('checkout_mercadopago_app');
