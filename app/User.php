<?php

namespace App;

class User{
    public $id, $name, $email, $legal_type, $document, $addresses, $favorites, $cart_products;
    protected $api_token;

    public function __construct($user){
        $this->id = $user->id;
        $this->name = $user->name;
        $this->email = $user->email;
        $this->api_token = $user->api_token;
        $this->legal_type = $user->legal_type;
        $this->document = $user->document;

        $this->cart_products = $this->cart_products();
        $this->favorites = $this->favorites();
    }

    public function update($data){
        $data['api_token'] = $this->api_token;

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/users/'.$this->id, ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                session(['user' => $response->data]);

                return ['success' => true, 'message' => 'Dados atualizados com sucesso.'];
            }
        }else{
            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado em sua requisição. Tente novamente em alguns minutos.'];
        }
    }

    public function cart_products(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/carts?with=product&api_token='.$this->api_token.'&search=user_id:'.$this->id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                return (object)['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return (object)['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function addToFavorites($product_id){
        $data = [
            'user_id' => $this->id,
            'product_id' => $product_id,
            'api_token' => $this->api_token
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/favorites', ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => 'true', 'message' => 'Produto adicionado aos favoritos com sucesso.'];
            }else{
                return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function removeFromFavorites($product_id){
        $data = [
            'api_token' => $this->api_token
        ];

        $favorite = collect($this->favorites)->where('product_id', $product_id)->first();

        if(!$favorite){
            return ['success' => 'true', 'message' => 'Produto removido dos favoritos com sucesso.'];
        }

        $client = new \GuzzleHttp\Client();
        $response = $client->delete('https://imarket.digital/api/favorites/'.$favorite->id, ['json' => $data]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => 'true', 'message' => 'Produto removido dos favoritos com sucesso.'];
            }else{
                return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function favorites(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/favorites?with=product&api_token='.$this->api_token.'&search=user_id:'.$this->id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                return (object)['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return (object)['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function removeFromCart($product_on_cart_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->delete('https://imarket.digital/api/carts/'.$product_on_cart_id, ['json' => ['api_token' => $this->api_token]]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => 'true', 'message' => 'Produto removido do carrinho com sucesso.', 'cart' => $this->cart_products()];
            }else{
                return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function updateCartProduct($product_on_cart_id, $quantity){
        if($quantity == 0){
            return $this->removeFromCart($product_on_cart_id);
        }

        $cart = [
            'quantity' => $quantity,
            'api_token' => $this->api_token
        ];

        $client = new \GuzzleHttp\Client();
        $response = $client->put('https://imarket.digital/api/carts/'.$product_on_cart_id, ['json' => $cart]);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return ['success' => 'true', 'message' => 'Carrinho atualizado com sucesso.', 'cart' => $this->cart_products()];
            }else{
                return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
        }
    }

    public function addToCart($product_id, $market_id, $quantity){
        $actual_cart = collect($this->cart_products());
        $product_on_cart = $actual_cart->where('product_id', $product_id)->first();
        $validMarket = ($actual_cart->count() > 0 && !$actual_cart->where('product.market_id', $market_id)->first()) ? false : true;

        if(!$validMarket){
            return ['success' => false, 'message' => 'Você deve esvaziar o seu carrinho para comprar em outro mercado.', 'invalid_market' => true];
        }

        // Caso o produto já esteja no carrinho não pode adicionar denovo
        if($product_on_cart){
            return $this->updateCartProduct($product_on_cart->id, $quantity);
        }

        if($quantity > 0) {
            $cart = [
                'user_id' => $this->id,
                'product_id' => $product_id,
                'quantity' => $quantity,
                'api_token' => $this->api_token
            ];

            $client = new \GuzzleHttp\Client();
            $response = $client->post('https://imarket.digital/api/carts', ['json' => $cart]);

            if($response->getStatusCode() == 200){
                $response = json_decode($response->getBody());

                if($response->success){
                    return ['success' => 'true', 'message' => 'Produto adicionado ao carrinho com sucesso.', 'cart' => $this->cart_products()];
                }else{
                    return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
                }
            }else{
                return ['success' => 'false', 'message' => 'Aconteceu algum erro inesperado. Tente novamente em alguns minutos.'];
            }
        }else{
            return ['success' => 'false', 'message' => 'Você não pode adicionar o produto ao carrinho sem selecionar a quantidade.'];
        }
    }

    public function getAddresses(){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/delivery_addresses?search=user_id:'.$this->id.'&api_token='.$this->api_token);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                 return $response->data;
            }else{
                session(['api_token' => null]);
            }
        }else{
            return (object)['success' => false, 'message' => 'Erro ao recuperar endereços do cliente.'];
        }
    }

    public function registerAddress($data){
        $data['api_token'] = $this->api_token;

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/delivery_addresses', ['json' => $data]);

        return $response->getBody();
    }

    public function getDeliveryAddress($id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/delivery_addresses/'.$id.'?api_token='.$this->api_token);

            if($response->getStatusCode() == 200){
                $response = json_decode($response->getBody());

                if($response->success){
                    return $response->data;
                }else{
                    session(['api_token' => null]);
                }
            }else{
                return (object)['success' => false, 'message' => 'Erro ao recuperar endereço do cliente.'];
            }
        }catch (\Exception $e){
            return (object)['success' => false, 'message' => 'Erro ao recuperar endereço do cliente.'];
        }
    }

    /*protected function find($api_token){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/user?api_token='.$this->api_token);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                $this->user = $response->data;
            }else{
                session(['api_token' => null]);
            }
        }else{
            session(['api_token' => null]);
        }
    }*/
}
