<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\LoginRequest;
use App\Http\Requests\RegisterRequest;

use App\User;
use App\Market;

use View;

use MPSdk;
use MPPreference;
use MPItem;

class WebsiteController extends Controller
{
    protected $user, $market, $cart_products, $favorites;
    protected $userLoggedIn = false;

    public function __construct(){
        $this->middleware(function($request, $next){
            if(session('user')){
                $user = new User(session('user'));

                if($user){
                    $this->userLoggedIn = true;
                    $this->user = $user;
                    $this->cart_products = $user->cart_products;
                    $this->favorites = $user->favorites;
                }
            }
            if(session('market_id')){
                $market = new Market(session('market_id'));

                if($market){
                    $this->market = $market;
                }
            }

            View::share('userLoggedIn', $this->userLoggedIn);
            View::share('cart_products', $this->cart_products);
            View::share('market', $this->market);
            View::share('favorites', $this->favorites);

            return $next($request);
        });
    }

    public function index(){
        return view('website.index');
    }

    public function changeMarket($market_id = null){
        session(['market_id' => $market_id]);

        if($market_id == null){
            return redirect()->route('markets-list');
        }else{
            return redirect()->route('market', [$market_id]);

        }
    }

    public function marketsList(){
        return view('website.markets-list');
    }

    public function market($market_id, $category_id = 0){
        return view('website.market', compact('market_id', 'category_id'));
    }

    public function cartClearMessage(){
        if(!$this->userLoggedIn){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return view('cart-clear-message');
    }

    public function cart(){
        return view('website.cart');
    }

    public function cartClear(){

    }

    public function checkout(){
        if(!$this->userLoggedIn){
            return redirect()->route('login-register');
        }
        if(!$this->cart_products){
            return redirect()->route('cart');
        }

        $user = $this->user;
        $user_addresses = $user->getAddresses();

        return view('website.checkout', compact('user_addresses', 'user'));
    }

    public function loginRegister(){
        return view('website.login-register');
    }

    public function login(LoginRequest $request){
        $credentials = $request->all();

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/login', ['json' => $credentials]);

        if($response->getStatusCode() != 200){
            return ['success' => false, 'message' => 'Credenciais inválidas, verifique os dados digitados e tente novamente.'];
        }else{
            $response = json_decode($response->getBody());

            if($response && $response->success){
                session(['user' => $response->data]);

                return ['success' => true];
            }

            return ['success' => false, 'message' => 'Credenciais inválidas, verifique os dados digitados e tente novamente.'];
        }
    }

    public function register(RegisterRequest $request){
        $user = $request->all();

        $client = new \GuzzleHttp\Client();
        $response = $client->post('https://imarket.digital/api/register', ['json' => $user]);

        if($response->getStatusCode() == 401){
           return ['success' => false, 'message' => 'O e-mail ou o documento já está cadastrado em nosso sistema.'];
        }else{
            $response = json_decode($response->getBody());

            if($response->success){
                session(['user' => $response->data]);

                return ['success' => true];
            }

            return ['success' => false, 'message' => 'Aconteceu algum erro inesperado em sua requisição. Tente novamente em alguns minutos.'];
        }
    }

    public function updateUser(UpdateUserRequest $request){
        return $this->user->update($request->only('name'));
    }

    public function profile(){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return view('website.profile', compact('user'));
    }

    public function favorites(){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        $favorites = $user->favorites();

        return view('website.favorites', compact('user', 'favorites'));
    }

    public function logout(){
        session(['user' => null]);

        return redirect()->route('markets-list');
    }

    public function register_address(Request $request){
        if($this->userLoggedIn){
            $data = [
                'zipcode' => $request->zipcode,
                'street' => $request->street,
                'number' => $request->number,
                'district' => $request->district,
                'complement' => $request->complement,
                'state_initials' => $request->state_initials,
                'city' => $request->city,
                'description' => $request->description,
                'address' => $request->street.', '.$request->number.', '.$request->complement.', '.$request->city.'-'.$request->state_initials.' CEP: '.$request->zipcode,
                'user_id' => $this->user->id
            ];

            $service_return = $this->user->registerAddress($data);

            return $service_return;
        }else{
            return json_encode(['success' => 'false', 'message' => 'Sua sessão expirou, efetue o login novamente.']);
        }
    }

    public function addProductToFavorites(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->addToFavorites($request->product_id);
    }

    public function removeProductFromFavorites(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->removeFromFavorites($request->product_id);
    }

    public function addProductToCart(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->addToCart($request->product_id, $request->market_id, $request->quantity);
    }

    public function mercadopago_app($api_token, $delivery_address_id){
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->get('https://imarket.digital/api/user', ['json' => ['api_token' => $api_token]]);
        }catch(\Exception $e){
            return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente efetuar o login novamente.']);
        }

        if($response->getStatusCode() == 200){
            $data = json_decode($response->getBody());
            if($data->success == true){
                $user = $data->data;
                session(['user' => $user]);
                session(['checkout_token' => md5(uniqid())]);
                $this->user = new User($user);

                $cart_products = $this->user->cart_products();
                if(count($cart_products) > 0){
                    $delivery_address = $this->user->getDeliveryAddress($delivery_address_id);
                    if(isset($delivery_address->user_id) && $delivery_address->user_id == $this->user->id){
                        $delivery_fee = $cart_products[0]->product->market->delivery_fee;
                        $products_total = 0;
                        $mp_products;
                        MpSDK::setAccessToken('TEST-156353036787675-100120-74f2da8b7066a98e75e9f66ad4b8a439-340744974');
                        $preference = new MPPreference();

                        foreach($cart_products as $prod){
                            $products_total += ($prod->quantity * $prod->product->price);

                            $item = new MPItem();
                            $item->title = $prod->product->name;
                            $item->quantity = $prod->quantity;
                            $item->unit_price = $prod->product->price;
                            $mp_products[] = $item;
                        }

                        if($delivery_fee > 0){
                            $item = new MPItem();
                            $item->title = 'Taxa de entrega';
                            $item->quantity = 1;
                            $item->unit_price = $delivery_fee;
                            $mp_products[] = $item;
                        }

                        $preference->external_reference = session('checkout_token');
                        $preference->items = $mp_products;
                        $preference->payment_methods = array(
                            "excluded_payment_types" => array(
                                array("id" => "ticket")
                            )
                        );
                        $preference->binary_mode = true;
                        $preference->save();

                        return view('website/mercadopago', compact('cart_products', 'delivery_address', 'delivery_fee', 'products_total', 'preference'));
                    }else{
                        return view('website/mercadopago_error')->with(['error_message' => 'Requisição inválida. Tente novamente.']);
                    }
                }
            }else{
                return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
            }
        }else{
            return view('website/mercadopago_error')->with(['error_message' => 'Não foi possível requisitar os dados ao servidor. Tente novamente.']);
        }

    }

    public function removeProductFromCart(Request $request){
        $user = $this->user;

        if(!$user){
            return redirect()->back()->with('error', 'Você precisa estar logado para acessar essa página');
        }

        return $user->removeFromCart($request->cart_id);
    }

    function hasProductsOnCart(){
        if($this->userLoggedIn && $this->cart_products && count($this->cart_products) > 0){
            return true;
        }

        return false;
    }
}
