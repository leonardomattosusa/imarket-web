<?php

namespace App;

class Market{
    public $id, $name, $delivery_fee;

    public function __construct($market_id){
        $market = $this->find($market_id);

        $this->id = $market->id;
        $this->name = $market->name;
        $this->delivery_fee = $market->delivery_fee;
        $this->photo = $market->has_media ? $market->media[0]->url : 'https://imarket.digital/images/image_default.png';
    }

    protected function find($market_id){
        $client = new \GuzzleHttp\Client();
        $response = $client->get('https://imarket.digital/api/markets/'.$market_id);

        if($response->getStatusCode() == 200){
            $response = json_decode($response->getBody());

            if($response->success){
                return $response->data;
            }else{
                session(['market_id' => null]);
            }
        }else{
            session(['market_id' => null]);
        }
    }
}
