<div class="single-category" id="category-template">
    <div class="category-image">
        <a href="#" data-attributes="href,title,class" data-variables="link,name,highlighted" title="">
            <img src="#!" data-attributes="src" data-variables="photo" class="img-fluid">
        </a>
    </div>
    <div class="category-title">
        <h3>
            <a href="#!" data-attributes="href,text" data-variables="link,name"></a>
        </h3>
    </div>
</div>
