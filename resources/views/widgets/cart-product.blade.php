<div class="cart-float-single-item d-flex" id="cart-product-template">
    <span class="remove-item"><a href="javascript:void(0)" cart_id="#" data-attributes="cart_id" data-variables="id" onclick="removeProductFromCart(this)"><i class="fa fa-times"></i></a></span>
    <div class="cart-float-single-item-image">
        <a href="javascript:void(0)" product_id="#" data-attributes="product_id" data-variables="product_id" data-toggle="modal" data-target="#quick-view-modal-container"><img src="#" data-attributes="src" data-variables="photo" class="img-fluid" alt=""></a>
    </div>
    <div class="cart-float-single-item-desc">
        <p class="product-title"> <a href="javascript:void(0)" product_id="#" data-toggle="modal" data-target="#quick-view-modal-container" data-attributes="text,product_id" data-variables="name,product_id"></a></p>
        <p class="price" data-attributes="html" data-variables="quantity_price"></p>
    </div>
</div>
