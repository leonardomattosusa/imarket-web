<div id="market-template">
    <div class="single-tab-slider-item">
        <div class="gf-product tab-slider-sub-product">
            <div class="image align-items-center">
                <a href="#!" data-attributes="href" data-variables="link">
                    <div data-attributes="style-image" data-variables="photo" class="img-fluid"></div>
                </a>
            </div>
            <div class="product-content d-flex flex-column flex-grow-1 justify-content-between">
                <h3 class="product-title"><a href="single-product.html" data-attributes="href,text" data-variables="link,name"></a></h3>
                <p class="product-description" data-attributes="text" data-variables="description"></p>

                <div class="market-button">
                    <a href="#!" data-attributes="href" data-variables="link"><button>Acessar mercado</button></a>
                </div>
            </div>
        </div>
    </div>
</div>
