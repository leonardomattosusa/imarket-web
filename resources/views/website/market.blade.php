@extends('website.layout.default')

@section('title', 'Nome do Mercado')

@section('stylesheets')
    <style>
        .image a div{
            min-height: 200px !important;
        }
    </style>
@endsection

@section('content')
<div class="slider category-slider mb-35">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="shop-header mb-35">
                    <div class="row">
                        {{--<div class="col-lg-4 col-md-4 col-sm-12 d-flex align-items-center">
                            <div class="sort-by-dropdown d-flex align-items-center mb-xs-10">
                                <p class="mr-10">Ordenar por: </p>
                                <select name="sort-by" id="sort-by" class="nice-select">
                                    <option value="0">Popularidade</option>
                                    <option value="0">Preço: Menor ao Maior</option>
                                    <option value="0">Preço: Maior ao Menor</option>
                                </select>
                            </div>
                        </div>--}}
                        <div class="col-lg-12 col-md-12 col-sm-12 d-flex flex-column flex-sm-row justify-content-between justify-content-sm-center align-items-left align-items-sm-center">
                            <div class="sort-by-dropdown d-flex flex-md-grow-1 flex-grow-0 justify-content-md-start justify-content-center align-items-center mb-xs-10">
                                <div class="header-advance-search">
                                    <form action="javascript:(searchProducts(this))">
                                        <input type="text" class="search-product" placeholder="Pesquisar por um produto" style="min-width: 220px" minlength="3">
                                        <button class="search-product-button">
                                            <i class="fa fa-check"></i>
                                        </button>
                                    </form>
                                </div>
                            </div>

                            <p class="result-show-message text-center text-md-right">Mostrando <span class="page-count">0</span> de <span class="page-total-count">0</span> resultados</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Categorias</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="category-slider-container" id="categories-list">
                    <div class="single-category" style="max-width: 250px">
                        <div class="category-image">
                            <a href="{{ url('market/'.$market_id.'/categories/0') }}" title="Todos os Produtos" {{ request()->segment('3') == 'categories' && request()->segment('4') == 0 ? 'class=highlighted' : '' }}>
                                <img src="https://imarket.digital/images/image_default.png" class="img-fluid">
                            </a>
                        </div>
                        <div class="category-title">
                            <h3>
                                <a href="{{ url('market/'.$market_id.'/categories/0') }}">Todos os Produtos</a>
                            </h3>
                        </div>
                    </div>

                    {{--Categories will be printed here--}}
                </div>
            </div>
        </div>
    </div>
</div>

@if(request()->segment('3') == null)
    <div class="slider tab-slider mb-35" id="featured-container">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h3>Destaques</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="tab-slider-wrapper bg-white shadow" style="padding: 0px 20px">
                        <div class="row" id="featured-list">
                            {{--Featured products will be printed here--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif

<div class="slider tab-slider mb-35">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Produtos</h3>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="tab-slider-wrapper bg-white shadow" style="padding: 0px 20px">
                    <div class="row" id="products-list">
                        {{--Products will be printed here--}}
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-30">
            <div class="col-12">
                <p class="result-show-message text-center text-md-right">Mostrando <span class="page-count">0</span> de <span class="page-total-count">0</span> resultados</p>
            </div>
        </div>
    </div>
</div>

{{--
<div class="slider brand-logo-slider mb-35">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="section-title">
                    <h3>Parceiros</h3>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="brand-logo-wrapper pt-20 pb-20">
                    <div class="col">
                        <div class="single-brand-logo">
                            <a href="#">
                                <img src="{{ asset('assets/images/brands/brand1.png') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="single-brand-logo">
                            <a href="#">
                                <img src="{{ asset('assets/images/brands/brand2.png') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="single-brand-logo">
                            <a href="#">
                                <img src="{{ asset('assets/images/brands/brand3.png') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="single-brand-logo">
                            <a href="#">
                                <img src="{{ asset('assets/images/brands/brand4.png') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="single-brand-logo">
                            <a href="#">
                                <img src="{{ asset('assets/images/brands/brand5.png') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>

                    <div class="col">
                        <div class="single-brand-logo">
                            <a href="#">
                                <img src="{{ asset('assets/images/brands/brand6.png') }}" class="img-fluid" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>--}}
@endsection

@section('scripts')
    <script>
        var categories, products;

        // Listagem de categorias
        $.ajax({
            url: 'https://imarket.digital/api/markets/' + market.id + '/categories',
            async: false,
            success: function(response){
                if(response.success){
                    categories = response.data;
                    actual_category_id = {{ $category_id }};

                    $.each(response.data, function(index, result){
                        result.link = window.location.origin + '/market/' + market.id + '/categories/' + result.id;
                        result.media[0] ? result.photo = result.media[0].url : undefined;
                        result.highlighted = (result.id == actual_category_id) ? 'highlighted' : null;

                        createElementFromTemplate($("#category-template"), result, $("#categories-list"));
                    });
                }
            }
        });

        // Listagem de destaques
        @if(request()->segment('3') == null)
            $.ajax({
                url: 'https://imarket.digital/api/products/?with=category&search=market_id:' + market.id + ';featured:1&searchJoin=and',
                async: false,
                success: function(response){
                    if(response.success){
                        products = response.data;

                        if(products.length > 0){
                            products_count = products.length;

                            updateCount(products_count);

                            $.each(response.data, function(index, result){
                                result = prepareProductObject(result);

                                createElementFromTemplate($("#product-template"), result, $("#featured-list"));
                            });
                        }else{
                            $("#featured-container").remove();
                        }
                    }
                }
            });
        @endif

        getMarketProducts();
    </script>
@endsection
