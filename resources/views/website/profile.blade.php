@extends('website.layout.default')

@section('content')
    <div class="my-account-section section position-relative mb-50 fix">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-lg-3 col-12">
                            <div class="myaccount-tab-menu nav" role="tablist">
                                <a href="#dashboad" class="active" data-toggle="tab"><i class="fa fa-dashboard"></i> Conta</a>
                                <a href="#orders" data-toggle="tab"><i class="fa fa-cart-arrow-down"></i> Meus Pedidos</a>
                                {{--<a href="#payment-method" data-toggle="tab"><i class="fa fa-credit-card"></i> Métodos de Pagamento</a>--}}
                                <a href="#address-edit" data-toggle="tab"><i class="fa fa-map-marker"></i> Meus endereços</a>

                                <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Sair</a>
                            </div>
                        </div>
                        <div class="col-lg-9 col-12">
                            <div class="tab-content" id="myaccountContent">
                                <div class="tab-pane fade show active" id="dashboad" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Conta</h3>

                                        <div class="welcome mb-4">
                                            <p>Olá, <strong>{{ $user->name }}</strong>, você pode atualizar seus dados básicos através do formulário abaixo:</p>
                                        </div>

                                        <div class="account-details-form">
                                            <form action="javascript:(updateUser())">
                                                <div class="row">
                                                    <div class="col-12">
                                                        <small class="text-danger" id="update-user-error-container"></small>
                                                    </div>
                                                    <div class="col-md-12 col-12 mb-20">
                                                        <label>Nome</label>
                                                        <input class="mb-0" id="name-input" type="text" placeholder="Nome completo" value="{{ $user->name }}">
                                                    </div>
                                                    <div class="col-md-12 mb-20">
                                                        <label>Endereço de email*</label>
                                                        <input class="mb-0" id="email-input" type="email" placeholder="Endereço de e-mail" value="{{ $user->email }}" disabled>
                                                    </div>
                                                    <div class="col-md-6 col-12 mb-20">
                                                        <label>Tipo de pessoa</label>
                                                        <select class="mb-0" id="legal-type-select" onchange="changeUserType()" disabled>
                                                            <option value="1" {{ ($user->legal_type == 1) ? 'selected' : '' }}>Pessoa física</option>
                                                            <option value="2" {{ ($user->legal_type == 2) ? 'selected' : '' }}>Pessoa jurídica</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-6 col-12 mb-20">
                                                        <label id="document-label">CPF</label>
                                                        <input class="mb-0" id="document-input" type="text" placeholder="Documento de identificação" value="{{ $user->document }}" disabled>
                                                    </div>
                                                    <div class="col-12">
                                                        <div class="text-center">
                                                            <button class="register-button mt-0" id="update-user-button">Atualizar Dados</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="orders" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Meus pedidos</h3>

                                        <div class="myaccount-table table-responsive text-center">
                                            <table class="table table-bordered">
                                                <thead class="thead-light">
                                                <tr>
                                                    <th>No</th>
                                                    <th>Name</th>
                                                    <th>Date</th>
                                                    <th>Status</th>
                                                    <th>Total</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>1</td>
                                                        <td>Mostarizing Oil</td>
                                                        <td>Aug 22, 2018</td>
                                                        <td>Pending</td>
                                                        <td>$45</td>
                                                        <td><a href="cart.html" class="btn">View</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>2</td>
                                                        <td>Katopeno Altuni</td>
                                                        <td>July 22, 2018</td>
                                                        <td>Approved</td>
                                                        <td>$100</td>
                                                        <td><a href="cart.html" class="btn">View</a></td>
                                                    </tr>
                                                    <tr>
                                                        <td>3</td>
                                                        <td>Murikhete Paris</td>
                                                        <td>June 12, 2017</td>
                                                        <td>On Hold</td>
                                                        <td>$99</td>
                                                        <td><a href="cart.html" class="btn">View</a></td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                {{--<div class="tab-pane fade" id="payment-method" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Métodos de Pagamento</h3>

                                        <p class="saved-message">Nenhum método de pagamento cadastrado.</p>
                                    </div>
                                </div>--}}

                                <div class="tab-pane fade" id="address-edit" role="tabpanel">
                                    <div class="myaccount-content">
                                        <h3>Meus endereços</h3>

                                        <address>
                                            <p><strong>Alex Tuntuni</strong></p>
                                            <p>1355 Market St, Suite 900 <br>
                                                San Francisco, CA 94103</p>
                                            <p>Mobile: (123) 456-7890</p>
                                        </address>

                                        <a href="#" class="btn d-inline-block edit-address-btn"><i class="fa fa-edit"></i>Edit Address</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        function updateUser(){
            let result = getUserData()
            let button_text = $("#update-user-button").html();

            if(result.status == 'error'){
                showUpdateUserError(result.message);
            }else{
                let user_data = result.user;

                $.ajax({
                    url: "{{ route('update_user') }}",
                    method: 'POST',
                    headers: {
                        'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')
                    },
                    data: user_data,
                    beforeSend: function(response){
                        $("#update-user-error-container").empty();
                        $("#update-user-button").html('<i class="fa fa-spin fa-spinner"></i>');
                    },
                    success: function(response){
                        console.log(response);
                        if(response.success){
                            toastr.success('Dados atualizados com sucesso');
                        }else{
                            showUpdateUserError(response.message);
                        }
                    },
                    error: function(response){
                        console.log(response);
                        if(response.success){
                            toastr.success('Dados atualizados com sucesso');
                        }else{
                            response = response.responseJSON;

                            showValidationUserErrors(response.errors);
                        }
                    },
                    complete: function(){
                        $("#update-user-button").html(button_text);
                    }
                })
            }
        }

        function showUpdateUserError(){
            $("#update-user-error-container").text(message);
        }

        function showValidationUserErrors(errors){
            let message = '';

            $.each(errors, function(index, error){
                message += error[0] + '<br>';
            });

            $("#update-user-error-container").html(message);
        }

        function getUserData(){
            let user = new Object();

            user.name = $("#name-input").val();
            user.legal_type = $("#legal-type-select").val();
            user.document = $("#document-input").val().replace(/\D/g,'');

            if((user.legal_type == 1 && user.document.length != 11) || (user.legal_type == 2 && user.document.length != 14)){
                return {'status' : 'error', 'message' : 'Documento inválido.'};
            }

            return {'status' : 'success', 'user' : user};
        }
    </script>
@endsection
