@extends('website.layout.default')

@section('title', '- Checkout')

@section('stylesheets')
    <style>
        input[disabled], select[disabled]{
            background-color: #DDDDDD;
        }
    </style>
@endsection

@section('content')
    <div class="page-section section mb-50">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form action="javascript:(createOrder())" class="checkout-form">
                        <div class="row row-40" id="checkout-div">
                            <div class="col-lg-7 mb-10">
                                <div id="billing-form" class="mb-40">
                                    <h4 class="checkout-title">Meus dados</h4>
                                    <div class="row mb-20">
                                        <div class="col-12">
                                            <small class="text-danger" id="update-user-error-container"></small>
                                        </div>
                                        <div class="col-md-12 col-12 mb-20">
                                            <label>Nome</label>
                                            <input class="mb-0" id="name-input" type="text" placeholder="Nome completo" value="{{ $user->name }}" disabled>
                                        </div>
                                        <div class="col-md-12 mb-20">
                                            <label>Endereço de email*</label>
                                            <input class="mb-0" id="email-input" type="email" placeholder="Endereço de e-mail" value="{{ $user->email }}" disabled>
                                        </div>
                                        <div class="col-md-6 col-12 mb-20">
                                            <label>Tipo de pessoa</label>
                                            <select class="mb-0" id="legal-type-select" onchange="changeUserType()" disabled>
                                                <option value="1" {{ ($user->legal_type == 1) ? 'selected' : '' }}>Pessoa física</option>
                                                <option value="2" {{ ($user->legal_type == 2) ? 'selected' : '' }}>Pessoa jurídica</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 col-12 mb-20">
                                            <label id="document-label">CPF</label>
                                            <input class="mb-0" id="document-input" type="text" placeholder="Documento de identificação" value="{{ $user->document }}" disabled>
                                        </div>
                                    </div>
                                    <h4 class="checkout-title">Endereço de entrega</h4>
                                    @forelse($user_addresses as $add)
                                        <div class="row mx-1">
                                            <div class="col-12 pt-3 pb-2 mb-2" style="background-color: #ddd">
                                                <div class="single-method">
                                                    <input type="radio" id="shipping_address_{{ $add->id }}" name="shipping_address" value="{{ $add->id }}" {{ $add->is_default == 1 ? 'checked' : '' }}>
                                                    <label for="shipping_address_{{ $add->id }}"><h4 class="font-weight-bold">{{ $add->description ? $add->description : "Sem nome" }}</h4></label>
                                                    <p data-method="{{ $add->id }}" style="display: {{ $add->is_default == 1 ? 'block' : 'none' }}">{{ $add->address }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    @empty
                                        <div class="row">
                                            <div class="col-12">
                                                <p class="text-center">Nenhum endereço cadastrado</p>
                                            </div>
                                        </div>
                                    @endforelse
                                    <div class="row">
                                        <div class="col-12 mt-4 d-flex align-items-center justify-content-center">
                                            <button class="btn btn-dark" data-toggle="modal" data-target="#address_modal">Cadastrar novo endereço</button>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-5">
                                <div class="row">
                                    <div class="col-12 mb-60">
                                        <h4 class="checkout-title">Carrinho</h4>

                                        <div class="checkout-cart-total">
                                            <h4>Produto <span>Total</span></h4>

                                            <ul>
                                                @php($subtotal = 0)
                                                @forelse($cart_products as $p)
                                                    @php($subtotal += $p->product->price * $p->quantity)
                                                    <li>{{ $p->product->name }} X {{ $p->quantity }} <span>R${{ number_format(($p->product->price * $p->quantity), 2,',','.') }}</span></li>
                                                @empty
                                                    <li>Nenhum produto adicionado ao carrinho.</li>
                                                @endforelse
                                            </ul>

                                            <p>Subtotal <span>R${{ number_format($subtotal, 2,',','.') }}</span></p>
                                            <p>Frete <span>R${{ number_format($market->delivery_fee, 2,',','.') }}</span></p>
                                            <h4>Valor total <span>R${{ number_format($subtotal + $market->delivery_fee, 2,',','.') }}</span></h4>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <h4 class="checkout-title">Método de pagamento</h4>

                                        <div class="checkout-payment-method">
                                            <div class="single-method">
                                                <input type="radio" id="payment_cash" name="payment-method" value="cash">
                                                <label for="payment_cash">Dinheiro na entrega</label>
                                                <p data-method="cash">Efetue o pagamento com dinheiro na entrega do pedido.</p>
                                            </div>
                                            <div class="single-method">
                                                <input type="radio" id="payment_payoneer" name="payment-method" value="payoneer">
                                                <label for="payment_payoneer">Cartão de Crédito</label>
                                                <p data-method="payoneer">Efetue o pagamento com cartão de crédito através de nossa plataforma de pagamento online.</p>
                                            </div>
                                            <div class="single-method mt-30">
                                                <input type="checkbox" name="accept_terms" id="accept_terms">
                                                <label for="accept_terms" style="text-transform: none; color: #222222">Li e aceito os <a href="#!">termos de contrato</a></label>
                                            </div>
                                        </div>

                                        <button class="place-order float-right">Prosseguir</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" role="dialog" tabindex="-1" id="address_modal">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <form onsubmit="return registerAddress()" class="checkout-form" id="registerAddressForm">
                    <div class="modal-header">

                    </div>
                    <div class="modal-body">
                        <div class="row" id="new_address">
                            <div class="col-12 mb-10">
                                <label>Identificação do endereço*</label>
                                <input type="text" name="description" placeholder="Ex: Casa, Trabalho, etc" required>
                            </div>

                            <div class="col-12 mb-10">
                                <label>CEP*</label>
                                <input type="text" class="cep_mask" name="zipcode" placeholder="00000-000" required>
                            </div>

                            <div class="col-md-8 col-12 mb-10">
                                <label>Logradouro*</label>
                                <input type="text" placeholder="Logradouro" name="street" id="logradouro" required>
                            </div>

                            <div class="col-md-4 col-12 mb-10">
                                <label>Número*</label>
                                <input type="text" placeholder="Número" name="number" required>
                            </div>

                            <div class="col-md-6 col-12">
                                <label>Bairro*</label>
                                <input type="text" placeholder="Bairro" name="district" id="bairro" required>
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Complemento</label>
                                <input type="text" placeholder="Complemento" name="complement">
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Estado*</label>
                                <select name="state_initials" id="uf">
                                    <option value="">Selecione</option>
                                    <option value="AC">Acre</option>
                                    <option value="AL">Alagoas</option>
                                    <option value="AP">Amapá</option>
                                    <option value="AM">Amazonas</option>
                                    <option value="BA">Bahia</option>
                                    <option value="CE">Ceará</option>
                                    <option value="DF">Distrito Federal</option>
                                    <option value="ES">Espírito Santo</option>
                                    <option value="GO">Goiás</option>
                                    <option value="MA">Maranhão</option>
                                    <option value="MT">Mato Grosso</option>
                                    <option value="MS">Mato Grosso do Sul</option>
                                    <option value="MG">Minas Gerais</option>
                                    <option value="PA">Pará</option>
                                    <option value="PB">Paraíba</option>
                                    <option value="PR">Paraná</option>
                                    <option value="PE">Pernambuco</option>
                                    <option value="PI">Piauí</option>
                                    <option value="RJ">Rio de Janeiro</option>
                                    <option value="RN">Rio Grande do Norte</option>
                                    <option value="RS">Rio Grande do Sul</option>
                                    <option value="RO">Rondônia</option>
                                    <option value="RR">Roraima</option>
                                    <option value="SC">Santa Catarina</option>
                                    <option value="SP">São Paulo</option>
                                    <option value="SE">Sergipe</option>
                                    <option value="TO">Tocantins</option>
                                </select>
                            </div>

                            <div class="col-md-6 col-12 mb-10">
                                <label>Cidade*</label>
                                <input type="text" placeholder="Cidade" name="city" id="cidade" required>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="col-12">
                            <button type="submit" class="place-order float-right mt-2" id="regiterAddressButton">Cadastrar</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.cep_mask').on('focusout', function(){
            var cep = $(this).val()
            var script = document.createElement('script')
            script.src = 'https://viacep.com.br/ws/'+ cep + '/json/?callback=callback_cep'
            document.body.appendChild(script)
        })

        function callback_cep(conteudo) {
            console.log(conteudo)
            if (!("erro" in conteudo)) {
                //Atualiza os campos com os valores.
                document.getElementById('logradouro').value=(conteudo.logradouro);
                document.getElementById('bairro').value=(conteudo.bairro);
                document.getElementById('cidade').value=(conteudo.localidade);
                document.getElementById('uf').value=(conteudo.uf);
            }
        }

        function registerAddress(){
            $.ajax({
                url: '{{ route('register_address') }}',
                headers: {'X-CSRF-TOKEN' : $('meta[name="csrf-token"]').attr('content')},
                data: $('#registerAddressForm').serialize(),
                method: 'POST',
                beforeSend(){
                    setButtonState('#regiterAddressButton', 'inactive')
                },
                success(data){
                    console.log(data)
                    setButtonState('#regiterAddressButton', 'active')
                    if(data.success == true){
                        $('#address_modal').modal('hide')
                        $('.checkout-form').load('/ #checkout-div')
                    }
                },
                error(data){
                    console.log(data)
                    setButtonState('#regiterAddressButton', 'active')
                }
            })

            return false
        }

        function setButtonState(selector, state){
            if(state == 'inactive'){
                $(selector).html('Enviando...')
                $(selector).prop('disabled', true)
                $(selector).addClass('btn-dark')
            }
            if(state == 'active'){
                $(selector).html('Cadastrar')
                $(selector).prop('disabled', false)
                $(selector).removeClass('btn-dark')
            }
        }

        function createOrder(){

        }
    </script>
@endsection
